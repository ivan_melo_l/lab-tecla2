/*
 * File:   main_teclado.c
 * Author: Alejandra Ramirez, Daniela Echeverri, Angie Zambrano y Valentina Garz�n
 *
 * Created on 20 de octubre de 2021, 09:12 PM
 */
// PIC24FJ64GB002 Configuration Bit Settings

// 'C' source line config statements
#include "xc.h"
// CONFIG4
#pragma config DSWDTPS = DSWDTPSF       // DSWDT Postscale Select (1:2,147,483,648 (25.7 days))
#pragma config DSWDTOSC = LPRC          // Deep Sleep Watchdog Timer Oscillator Select (DSWDT uses Low Power RC Oscillator (LPRC))
#pragma config RTCOSC = SOSC            // RTCC Reference Oscillator  Select (RTCC uses Secondary Oscillator (SOSC))
#pragma config DSBOREN = ON             // Deep Sleep BOR Enable bit (BOR enabled in Deep Sleep)
#pragma config DSWDTEN = ON             // Deep Sleep Watchdog Timer (DSWDT enabled)

// CONFIG3
#pragma config WPFP = WPFP63            // Write Protection Flash Page Segment Boundary (Highest Page (same as page 42))
#pragma config SOSCSEL = IO             // Secondary Oscillator Pin Mode Select (SOSC pins have digital I/O functions (RA4, RB4))
#pragma config WUTSEL = LEG             // Voltage Regulator Wake-up Time Select (Default regulator start-up time used)
#pragma config WPDIS = WPDIS            // Segment Write Protection Disable (Segmented code protection disabled)
#pragma config WPCFG = WPCFGDIS         // Write Protect Configuration Page Select (Last page and Flash Configuration words are unprotected)
#pragma config WPEND = WPENDMEM         // Segment Write Protection End Page Select (Write Protect from WPFP to the last page of memory)

// CONFIG2
#pragma config POSCMOD = HS             // Primary Oscillator Select (HS Oscillator mode selected)
#pragma config I2C1SEL = PRI            // I2C1 Pin Select bit (Use default SCL1/SDA1 pins for I2C1 )
#pragma config IOL1WAY = ON             // IOLOCK One-Way Set Enable (Once set, the IOLOCK bit cannot be cleared)
#pragma config OSCIOFNC = ON            // OSCO Pin Configuration (OSCO pin functions as port I/O (RA3))
#pragma config FCKSM = CSECMD           // Clock Switching and Fail-Safe Clock Monitor (Sw Enabled, Mon Disabled)
#pragma config FNOSC = PRIPLL           // Initial Oscillator Select (Primary Oscillator with PLL module (XTPLL, HSPLL, ECPLL))
#pragma config PLL96MHZ = ON            // 96MHz PLL Startup Select (96 MHz PLL Startup is enabled automatically on start-up)
#pragma config PLLDIV = DIV12           // USB 96 MHz PLL Prescaler Select (Oscillator input divided by 12 (48 MHz input))
#pragma config IESO = OFF               // Internal External Switchover (IESO mode (Two-Speed Start-up) disabled)

// CONFIG1
#pragma config WDTPS = PS32768          // Watchdog Timer Postscaler (1:32,768)
#pragma config FWPSA = PR128            // WDT Prescaler (Prescaler ratio of 1:128)
#pragma config WINDIS = OFF             // Windowed WDT (Standard Watchdog Timer enabled,(Windowed-mode is disabled))
#pragma config FWDTEN = OFF             // Watchdog Timer (Watchdog Timer is disabled)
#pragma config ICS = PGx1               // Emulator Pin Placement Select bits (Emulator functions are shared with PGEC1/PGED1)
#pragma config GWRP = OFF               // General Segment Write Protect (Writes to program memory are allowed)
#pragma config GCP = OFF                // General Segment Code Protect (Code protection is disabled)
#pragma config JTAGEN = OFF             // JTAG Port Enable (JTAG port is disabled)

// #pragma config statements should precede project file includes.
// Use project enums instead of #define for ON and OFF.



#define _XTAL_FREQ 4000000
#define ANODOS PORTAbits.RA0
#define SEGMENTOS PORTB

//DEFINICION DE VARIABLES GLOBALES
char CONT_DIGITO = 0;
char DIG1 = 1;
char DIG2 = 2;
char DIG3 = 3;
char DIG4 = 4;
char Bandera = 5;


//******************************************************************************
// DECODIFICADOR DISPLAY 7 SEGMENTOS ANODO COMUN
// DP G F E D C B A
char DECO1(char NUM) {
    switch (NUM) {
        case 0:
            return 0xC0;
        case 1:
            return 0xF9;
        case 2:
            return 0xA4;
        case 3:
            return 0xB0;
        case 4:
            return 0x99;
        case 5:
            return 0x92;
        case 6:
            return 0x82;
        case 7:
            return 0xF8;
        case 8:
            return 0x80;
        case 9:
            return 0x90;
        case 10:
            return 0x47;
        case 11:
            return 0x62;
        case 12:
            return 0xFF; //APAGADO
        case 13:
            return 0x09;
        case 14:
            return 0x4F;
        case 15:
            return 0x86;
        case 16:
            return 0xBF;
        default:
            return 0xFF;
    }
}
//******************************************************************************
//Vizualizacion  
void ACTUALIZA_DISPLAY() {
    char TempSal;     
    
    TempSal = ANODOS&0x07;   //Lee el valor del Puerto del salida
    
    ANODOS = 0x00|TempSal; //APAGA TODOS LOS DIGITOS
    
    CONT_DIGITO++;
    CONT_DIGITO = CONT_DIGITO&3;
    
    switch (CONT_DIGITO) {
        case 0:
            SEGMENTOS = DECO1(DIG1);
            ANODOS = 0x80|TempSal; // 0111 0???
            break;
        case 1:
            SEGMENTOS = DECO1(DIG2);
            ANODOS = 0x40|TempSal; //1011 0???
            break;
        case 2:
            SEGMENTOS = DECO1(DIG3);
            ANODOS = 0x20|TempSal; //1101 0???
            break;
        case 3:
            SEGMENTOS = DECO1(DIG4);
            ANODOS = 0x10|TempSal; //1110 000?
            break;
        default:
            CONT_DIGITO = 0xFF;
            break;
    }
}
//******************************************************************************
//Lectura teclado
char TECLADO() {
    char Tecla = 1;
    char VPTOB = 0x0E;

    do {
        PORTB = VPTOB;
        if (PORTBbits.RB12 == 0) goto Antirrebotes;
        Tecla++;
        if (PORTBbits.RB13 == 0) goto Antirrebotes;
        Tecla++;
        if (PORTBbits.RB14 == 0) goto Antirrebotes;
        Tecla++;
        if (PORTBbits.RB15 == 0) goto Antirrebotes;
        Tecla++;
        VPTOB = (VPTOB << 1) | 1;
    } while (Tecla < 17);
    return 0;

Antirrebotes:
    while (PORTBbits.RB12 == 0) {
    };
    while (PORTBbits.RB13 == 0) {
    };
    while (PORTBbits.RB14 == 0) {
    };
    while (PORTBbits.RB15 == 0) {
    };
        switch (Tecla) {
        case 1:
            return '7';
        case 2:
            return '8';
        case 3:
            return '9';
        case 4:
            return 0x0A;
        case 5:
            return '4';
        case 6:
            return '5';
            break;
        case 7:
            return '6';
        case 8:
            return 0x0B;
        case 9:
            return '1';
        case 10:
            return '2';
        case 11:
            return '3';
        case 12:
            return 0x0C;
        case 13:
            return 0x0D;
        case 14:
            return '0';
        case 15:
            return 0x0E;
        case 16:
            return 0x0F;
    }
    return 0;
}
//******************************************************************************
// Main
void main(void) {
    char Temp1;
    char Flag;
    TRISB = 0xF0;
    
    while (1) 
    {
        __delay_ms(100);
        Temp1 = TECLADO();
        
    
        if ((Temp1>='0') && (Temp1<= '9') && (Bandera !=5)) {
            DIG4 = DIG3;
            DIG3 = DIG2;
            DIG2 = DIG1;
            DIG1 = Temp1 & 0x0F;
        }
        else if (Temp1==0x0A){
            // Modificar TL
            DIG1=12;
            DIG2=11;
            DIG3=0;
            DIG4=10 & 0x0F;
            
            Flag = Mod_TL();
            Bandera = Flag;
        }
        else if (Temp1==0x0B){
            // Modificar TH
            DIG1=12;
            DIG2=9;
            DIG3=14;
            DIG4=13 & 0x0F;
            
 
            Bandera = Flag;
        }
        else if (Temp1==0x0C){
            // Modificar Tiempo
            DIG1=12;
            DIG2=9;
            DIG3=15;
            DIG4=5 & 0x0F;
            Bandera = Flag;
        }   
     

        }
}       
